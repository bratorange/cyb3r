import bl00mbox
import sys_kernel
import math
import leds
import audio
from st3m.application import Application, ApplicationContext
import st3m.run

class Cyb3r(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        blm = bl00mbox.Channel()
        blm.volume = 6000
        audio.adjust_volume_dB(14)
        self.oc = blm.new(bl00mbox.patches.tinysynth)
        self.oc.signals.output = blm.mixer
        self.pitch_low = 22000
        self.pitch_high = 23000
        self.pitch_range = self.pitch_high - self.pitch_low
        leds.set_slew_rate(255)
        self.oc.signals.trigger.start()
        self.ui_interval = 10
        self.i=0
        
    def draw(self, ctx: Context) -> None:
        self.i+=1
        self.oc.signals.pitch = math.sin(self.i * .08) * self.pitch_range + self.pitch_low
        led_phase = int(self.i * .5) % 6
        if(led_phase == 1):
            leds.set_all_rgb(255, 0, 0)
        elif(led_phase == 5):
            leds.set_all_rgb(0, 0, 255)
        else:
            leds.set_all_rgb(255, 255, 255)
        leds.set_brightness(led_phase%2 *255)

        ctx.font = ctx.get_font_name(5)
        ctx.text_align = ctx.MIDDLE
        ctx.font_size = 100

        invert = self.i % self.ui_interval > (self.ui_interval*.5)
        if invert:
            ctx.rgb(255, 255, 255).rectangle(-120, -120, 240, 240).fill()
            ctx.move_to(0, -20).rgb(0, 0, 0).text('Cyber')
            ctx.move_to(0, 50).rgb(0, 0, 0).text('Police')
        else:
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
            ctx.move_to(0, -20).rgb(255, 255, 255).text('Cyber')
            ctx.move_to(0, 50).rgb(255, 255, 255).text('Police')

        leds.update()

if __name__ == "__main__":
    st3m.run.run_view(Cyb3r(ApplicationContext()))